# Overview

We'll be using Cilium with k3d instead of Calico and see how we can make some
network policies and use other features of Cilium.

# Pre-Requisites

## Preparing k3d

Cilium's `container-init.sh` script is `/bin/bash` based, but k3d uses BusyBox
as the base for their docker containers. Those containers have shell but not
bash. As a result, the install of that script will fail. To make the install
work we'll do some manual mounting for Cilium.

### Create a k3d Cluster

We're going to create a cluster and turn off the built in Flannel and K8s network policy
engine.

```shell
# Create a cluster without the built in CNI "Flannel"
k3d cluster create cilium-test --agents 1 --k3s-server-arg "--disable-network-policy" --k3s-server-arg "--flannel-backend=none"
```

Now let's check whether it installed properly ***(DON'T WORRY HERE, IT DID NOT!)***, and
you'll notice that everything's just pending.

```shell
kubectl get pods --all-namespaces --watch
```

The `Pending` state is because they're all daemonsets waiting to be installed on a bunch things
we turned off, but will be installing with Cilium.

### Mount BPF File System

BPF objects such as programs are accessed by user space via file descriptors (FDs), and each object has a reference counter. When an object is accessed, the kernel notes that by incrementing a counter `refcnt`, and decrements it when it's closed.

BPF objects have a lifecycle. If we want our BPF objects, or programs, to stay "alive" we need some way 
to "pin" them _(i.e. incrementing that `refcnt`)_. Typically, a user space process can do this, but in our case that won't work, since in networking we'll want it to work without a user space daemon. Instead we create a BPF File System (BPFFS) where the object can be stored and marked by the BPFFS via incrementing `refcnt`.

*Note: Much of the below install steps were discovered and graciously shared online by Sebastian Kurfürst with his article [Running Cilium in K3S and K3D (lightweight Kubernetes) on Mac OS for development
](https://sandstorm.de/de/blog/post/running-cilium-in-k3s-and-k3d-lightweight-kubernetes-on-mac-os-for-development.html).*

```shell
# First find all the nodes from docker. We'll need to create a mount
# for each one
cilium_nodes=(`docker ps --format "{{.Names}}" | grep "k3d-cilium" | awk -F "\n" '{print $1}'`)

# Now we're going to loop through each of them so we can mount a 
# BPF filesystem on each node
#
# Notice we're using `[@]` instead of `[*]` to treat `cilium_nodes` as
# an array. That's because `[@]` expands the array to have each element
# treated as a fully separate item, but `[*]` will separate things
# into single item with spaces between items.
#
# For the two docker commands, we're doing a few things:
#    1. Mounting a new BPF Filesystem at `/sys/fs/bpf/
#    2. Marking that mount as filesystem type `bpf`
#    3. Making that shared so that others can replicate it and any changes
#       made on those will propogate to this mount
#
for node in "${cilium_nodes[@]}"; \
do \
   docker exec -it "$node" mount bpffs /sys/fs/bpf -t bpf; \
   docker exec -it "$node" mount --make-shared /sys/fs/bpf; \
done

# Now let's check it worked 
for node in "${cilium_nodes[@]}"; \
do \
   docker exec -it "$node" mount | grep bpf; \
done

# It should print out something like:
#
# none on /sys/fs/bpf type bpf (rw,relatime)
# none on /sys/fs/bpf type bpf (rw,relatime)
# none on /sys/fs/bpf type bpf (rw,relatime)
```

# Installing Cilium

Next, we'll install Cilium using Helm.

```shell
# Add the repo so Helm can find the chart
helm repo add cilium https://helm.cilium.io/

# Install Cilium via the Helm chart
helm install cilium cilium/cilium --version 1.9.6 \
   --namespace kube-system \
   --set kubeProxyReplacement=partial \
   --set hostServices.enabled=false \
   --set externalIPs.enabled=true \
   --set nodePort.enabled=true \
   --set hostPort.enabled=true \
   --set bpf.masquerade=false \
   --set image.pullPolicy=IfNotPresent \
   --set ipam.mode=kubernetes

# Now let's make sure everything works
kubectl get pods --all-namespaces --watch

# You'll see a bunch of nodes all in different states and 0/1.
# Eventually, they'll all end up with 1/1 or 2/2 except for 
# one job which is a one-time job and "completed"
#
# You'll see something like:
#
# NAMESPACE     NAME                                      READY   STATUS      RESTARTS   AGE
# kube-system   cilium-operator-5ffcd8c5b6-c99pp          1/1     Running     0          70s
# kube-system   cilium-operator-5ffcd8c5b6-w55lq          1/1     Running     0          70s
# kube-system   cilium-sht7m                              1/1     Running     0          70s
# kube-system   cilium-7dn9f                              1/1     Running     0          70s
# kube-system   metrics-server-86cbb8457f-w8v6c           1/1     Running     0          5m57s
# kube-system   local-path-provisioner-5ff76fc89d-c82bh   1/1     Running     0          5m57s
# kube-system   coredns-854c77959c-kqc44                  1/1     Running     0          5m57s
# kube-system   helm-install-traefik-8pdj7                0/1     Completed   0          5m57s
# kube-system   svclb-traefik-m87dm                       2/2     Running     0          30s
# kube-system   svclb-traefik-nbjn4                       2/2     Running     0          30s
# kube-system   traefik-6f9cbd9bd4-hgj7b                  1/1     Running     0          30s
```

# Testing Cilium

## Installing Hubble

Cilium provides a UI based product called [Hubble](https://github.com/cilium/hubble) that
enables you to see what services are communicating, security and app monitoring, and more.

We're going to install it to do our first test and make sure Cilium is working.

```shell
# It's provided by the same Helm chart as the base Cilium product
# so we're going to upgrade that package
helm upgrade cilium cilium/cilium --version 1.9.6 \
  --namespace kube-system \
  --reuse-values \
  --set hubble.listenAddress=":4244" \
  --set hubble.relay.enabled=true \
  --set hubble.ui.enabled=true

# Now we need to forward the port so we can reach it from our host computer
kubectl port-forward -n kube-system svc/hubble-ui --address 0.0.0.0 --address :: 12000:80
```

Now go to a browser and view [http://localhost:12000/](http://localhost:12000/) and you should
see:

![Hubble UI](hubble_ui.png)

## Testing Cilium With Connectivity Test

Cilium provides a connectivity test that we can use to verify it was installed properly.
Let's install and run that:

```shell
# Create the namespace to run it in
kubectl create namespace cilium-test

# Deploy the test
kubectl apply -n cilium-test -f https://raw.githubusercontent.com/cilium/cilium/v1.9/examples/kubernetes/connectivity-check/connectivity-check.yaml
```

We'll now check it two ways:

1. Make sure all the pods ran without error
   * NOTE: If we had deployed the connectivity test to a single node cluster, 
     the pods that check multi-node functionality would stay `Pending`
1. Check Hubble to see everything looks good

```shell
kubectl get pods -n cilium-test

# You should see something like:
#
# NAME                                                    READY   STATUS    RESTARTS   AGE
# pod-to-external-fqdn-allow-google-cnp-f48574954-qf6x9   1/1     Running   0          2m14s
# echo-b-5884b7dc69-kpc29                                 1/1     Running   0          2m15s
# pod-to-b-multi-node-clusterip-cd4d764b6-jlslv           1/1     Running   0          2m14s
# pod-to-a-denied-cnp-75cb89dfd-wvzzh                     1/1     Running   0          2m14s
# pod-to-external-1111-d5c7bb4c4-6xg2d                    1/1     Running   0          2m15s
# pod-to-b-multi-node-headless-6696c5f8cd-4gcj4           1/1     Running   0          2m14s
# echo-a-dc9bcfd8f-g6stp                                  1/1     Running   0          2m15s
# host-to-b-multi-node-clusterip-c4ff7ff64-qsr5d          1/1     Running   0          2m13s
# pod-to-a-allowed-cnp-7d7c8f9f9b-v79sz                   1/1     Running   0          2m14s
# pod-to-a-5cdfd4754d-vd9ln                               1/1     Running   0          2m15s
# host-to-b-multi-node-headless-84d8f6f4c4-7cn6m          1/1     Running   0          2m13s
# echo-b-host-cfdd57978-966zm                             1/1     Running   0          2m15s
# pod-to-b-intra-node-nodeport-99b499f7d-8vwcc            1/1     Running   0          2m13s
# pod-to-b-multi-node-nodeport-7ff5595558-5rvc4           1/1     Running   0          2m13s
```
### Checking in Hubble

Now go back to Hubble in the browser and refresh it. Then choose the `cilium-test`
namespace from the dropdown in the upper left. You should see something like:

![cilium-test in Hubble UI](hubble_cilium_test.png)

Not that this is a live updating page.

# Using Cilium Networking